package com.example.data.local


import androidx.room.Dao
import androidx.room.Insert

import androidx.room.Query
import com.example.data.local.entities.CategoryEntity
import com.example.data.local.entities.MealDetailsEntity

@Dao
interface MealDao {

    @Insert
    suspend fun insertCategory(vararg category: CategoryEntity)

    @Query("SELECT * FROM Categories")
    suspend fun getCategories(): List<CategoryEntity>

    @Insert
    suspend fun insertMeal(vararg meal: MealDetailsEntity)

    @Query("SELECT * FROM Meals WHERE strCategory = :category")
    suspend fun getMealsByCategory(category: String): List<MealDetailsEntity>

    @Query("SELECT * FROM Meals WHERE idMeal = :id")
    suspend fun getMealById(id: String): MealDetailsEntity
}