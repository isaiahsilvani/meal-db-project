package com.example.data.remote

import com.example.data.remote.models.Categories
import com.example.data.remote.models.Meal
import com.example.data.remote.models.Meals
import retrofit2.http.GET
import retrofit2.http.Query

interface MealService {

    companion object {
        const val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"
        const val CATEGORIES_URL = "categories.php"
        const val MEALS_BY_CATEGORY_URL = "filter.php" // + ?c=beef
        const val DETAILS_URL = "lookup.php" // + ?i=52874
    }

    @GET(CATEGORIES_URL)
    fun getCategories(): Categories

    @GET(MEALS_BY_CATEGORY_URL)
    fun getMealsByCategory(@Query("c") c: String): Meals

    @GET(DETAILS_URL)
    fun getMealDetails(@Query("i") i: String): Meal

}