package com.example.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitProvider {
    private val retrofitObject: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())

    fun getMealService(): MealService =
        retrofitObject.baseUrl(MealService.BASE_URL)
            .build()
            .create()
}