package com.example.data.remote.models

data class Meal(
    val meals: List<MealDetails>
)