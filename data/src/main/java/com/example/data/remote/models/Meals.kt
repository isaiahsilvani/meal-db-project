package com.example.data.remote.models

data class Meals(
    val meals: List<Meal>
)