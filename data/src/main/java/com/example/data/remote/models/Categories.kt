package com.example.data.remote.models

data class Categories(
    val categories: List<Category>
)