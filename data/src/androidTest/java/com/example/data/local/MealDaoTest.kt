package com.example.data.local

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.data.local.entities.CategoryEntity
import com.example.data.local.entities.MealDetailsEntity
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class MealDaoTest {
    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: MealDao

    @Before
    fun setup() {
        // not a real database. Holds DB in RAM, not persistent storage
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.mealDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun get_Categories(): Unit = runBlocking {
        val categories = listOf(
            CategoryEntity("x", "y", "as", "as"),
            CategoryEntity("y", "x", "as", "as"),
        )
        dao.insertCategory(*categories.toTypedArray())
        val categoriesFromDB = dao.getCategories()
        assertThat(categoriesFromDB.containsAll(categories))
    }

    @Test
    fun get_Meals_By_Category_Returns_Meals(): Unit = runBlocking {
        val meals = listOf(
            MealDetailsEntity("12", "Tacos", "fdsfef", strCategory = "Beef"),
            MealDetailsEntity("40", "Filet", "sdfdsf", strCategory = "Beef"),
            MealDetailsEntity("60", "Chicken Breast", "fdsfef", strCategory = "Chicken"),
        )
        dao.insertMeal(*meals.toTypedArray())
        val mealsFromDB = dao.getMealsByCategory("Beef")
        assertThat(mealsFromDB.all { it.strCategory == "Beef" })
    }

    @Test
    fun get_Meal_By_Id_Returns_Meal(): Unit = runBlocking {
        val meals = listOf(
            MealDetailsEntity("40", "Filet", "sdfdsf", strCategory = "Beef"),
            MealDetailsEntity("60", "Chicken Breast", "fdsfsadfdsaef", strCategory = "Chicken"),
            MealDetailsEntity("96", "Apple Sauce", "fdsasdffef", strCategory = "Apples"),
        )
        dao.insertMeal(*meals.toTypedArray())
        val mealFromDB = dao.getMealById("60")
        assertThat(mealFromDB == meals[1])
    }

}